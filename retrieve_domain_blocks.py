#!/usr/bin/env python3

##### IMPORTS
import argparse
import gzip
from math import isnan
import pandas as pd
import re
import socket
from sys import exit
from typing import Optional
from urllib.error import HTTPError
from urllib.parse import quote, unquote, urlencode
from urllib.request import urlopen, Request

##### GLOBALS

domain_parse = re.compile( r'^https?://([^/]+\.[^/]+)/?' )
find_masto_session = re.compile( r'_mastodon_session=([^;]+)' )
find_auth_token = re.compile( r'<input type="hidden" name="authenticity_token" value="([^"]+)" autocomplete="off"' )
shared_headers = {
                        'User-Agent':'retrieve_domain_blocks.py',
                        'Accept':'text/html, application/xhtml+xml, application/xml;q=0.9, */*;q=0.8',
                        'Accept-Language':'en;q=1.0',
                        'Accept-Encoding':'gzip;q=1.0',
                        'Connection':'keep-alive',
                        'Upgrade-Insecure-Requests':'1',
                        'Sec-Fetch-Dest':'document',
                        'Sec-Fetch-Mode':'navigate',
                        'Sec-Fetch-Site':'same-origin',
                        'TE':'trailers',
                        'DNT':'1',
        }

##### FUNCTIONS

def grab_domains( url: str ) -> Optional[pd.DataFrame]:

    try:
        temp = pd.read_json(f'{url}/api/v1/instance/domain_blocks').fillna('')
        domain_name = domain_parse.match( url ).groups()[0]
        temp['source'] = [domain_name] * len(temp)
        return temp
    except HTTPError as e:
        return None

def merge_domains( frames: list[pd.DataFrame], maximize_severity: bool=True ) -> pd.DataFrame:

    first = frames[0]
    for second in frames[1:]:
        merged = pd.concat( [first, second], ignore_index=True )
        dupes  = merged.duplicated(subset=['digest'], keep=False)
        third  = merged[~dupes]
        
        handled = set()
        for idx,row in merged[dupes].iterrows():
            if row.digest in handled:
                continue

            first_row  = first.iloc[  first[  first.digest  == row.digest ].index[0] ]
            second_row = second.iloc[ second[ second.digest == row.digest ].index[0] ]
            merged_row = dict(digest = [row.digest])

            # drop censored/over-precise domains
            if (first_row.domain.count('*') > 1) or (len(first_row.domain) > len(second_row.domain)):
                merged_row['domain'] = [second_row.domain]
            else:
                merged_row['domain'] = [first_row.domain]

            # handle divergent severities
            if first_row.severity != second_row.severity:
                # KLUDGE: there's only two options!
                merged_row['severity'] = ['suspend' if maximize_severity else 'silence']
            else:
                merged_row['severity'] = first_row.severity

            # concat the comments together, if they exist
            if (first_row.comment is None) or (first_row.comment.strip() == ''):
                merged_row['comment'] = [second_row.comment]
            elif (second_row.comment is None) or (second_row.comment.strip() == ''):
                merged_row['comment'] = [first_row.comment]
            else:
                merged_row['comment'] = [first_row.comment + '; ' + second_row.comment]

            merged_row['source'] = first_row['source'] + ", " + second_row['source']

            third = pd.concat( [third, pd.DataFrame(merged_row)], ignore_index=True )
            handled.add( row.digest )

        first = third

    return first

def prune_domains( domains: pd.DataFrame, excludes: list[str] = list() ):

    if excludes is None:
        return domains

    temp = domains
    for domain in excludes:
        temp = temp[ temp.domain != domain ]
    return temp

def check_domains_DNS( domains: pd.DataFrame ):

    mask = list()
    for idx,row in domains.iterrows():
        try:
            address = socket.gethostbyname( row.domain )
            mask.append( True )
        except:
            mask.append( False )

    return domains[mask]

def print_domains( domains: pd.DataFrame, style=None, skip_silence=False ):

    # ship out the header first
    if style == 'condensed':
        maxi = {'severity':0, 'domain':0, 'source':0, 'comment':0}
        for idx,row in domains.iterrows():
            for key in maxi.keys():
                maxi[key] = max( maxi[key], len(row[key] if (type(row[key]) is str) else str(row[key])) )

        yield '  '.join( [key.rjust(maxi[key]) for key in maxi.keys()] ), None
    elif style == 'expanded':
        pass
    else:
        yield '#domain,#severity,#reject_media,#reject_reports,#private_comment,#public_comment,#obfuscate', None

    sort = domains.sort_values(['severity','domain'])
    for idx,row in sort.iterrows():
        if skip_silence and row.severity == 'silence':
            continue
        if style == 'expanded':
            yield f"""
{row.severity}
{row.domain}
{row.source}
{row.comment}
{row.digest}
""", row
        elif style == 'condensed':
            yield '  '.join( [row[key].ljust(maxi[key]) if ((type(row[key]) is str) and (row[key] != '')) \
                    else 'None'.ljust(maxi[key]) for key in maxi.keys()] ).strip(), row
        else:
            yield f'{row.domain},{row.severity},false,false,"{row.source}","{row.comment}",false', row


##### MAIN

if __name__ == '__main__':

    # handle the command line
    parser = argparse.ArgumentParser( description="Retrieve the domain blocks one or more Mastodon instances and print them." )

    parser.add_argument( '-f','--format', choices=['expanded','condensed','v4'], help="Print out all info, do one-line output, or something importable?" )
    parser.add_argument( '-m','--maximize_severity', action='store_true', help="If two servers disagree on severity, go with the maximal value." )
    parser.add_argument( '-s','--skip_silenced', action='store_true', help="Don't print out any 'silenced' domains." )
    parser.add_argument( '-d','--DNS_check', action='store_true', help="Do a DNS lookup to detect if the domain is censored." )
    parser.add_argument( '-o','--override', metavar="DOMAIN", nargs='*', help="Ignore any recommendation for these domains." )

    parser.add_argument( '--target', metavar="URI", help="The domain to upload these blocks to." )
    parser.add_argument( '--session_id', metavar="STRING", help="A valid persistent session cookie for that remote domain. NOT URI encoded!" )
    parser.add_argument( '--masto_session', metavar="STRING", help="A valid authorization token for that remote domain. NOT URI encoded!" )

    parser.add_argument( 'uri', metavar="URI", nargs='+', help="The URIs of the Mastodon server(s)." )

    args = parser.parse_args()

    # start grabbing from the domains
    domains = list()
    for uri in args.uri:

        result = grab_domains( uri )
        if result is not None:
            print( f"= Found {len(result)} domain(s) blocked at {uri}." )
            domains.append( result )

    if len(domains) == 0:
        print( "ERROR: None of the supplied Mastodon servers publish their domain block list." )
        exit(1)

    # consolidate them together
    merged = merge_domains( domains, args.maximize_severity )
    print( f"= After removing duplicates, {len(merged)} domains remained." )

    pruned = prune_domains( merged, args.override )
    print( f"= After removing overridden domains, {len(pruned)} domains remained." )

    if args.DNS_check:
        weeded = check_domains_DNS( pruned )
        print( f"= After doing some DNS checks, {len(weeded)} domains remained." )
    else:
        weeded = pruned

    print( "\n==CUT==" )

    # print them out
    gen = print_domains( weeded, args.format, args.skip_silenced )
    for block, row in gen:
        print( block )
                
        # if requested, try feeding values in
        if args.target and (row is not None):

        # build up a request to set up a new domain block
                outgoing = Request( f'{args.target}/admin/domain_blocks/new', headers=shared_headers | {
                        'Referer':f'{args.target}/admin/blocks',
                        'Cookie':f'_mastodon_session={quote(args.masto_session)}; _session_id={quote(args.session_id)}',
                        } )

                incoming = urlopen( outgoing )

                # parse out the auth token from the headers
                for key, value in incoming.headers.items():
                        if key != 'set-cookie':
                                continue
                        search = find_masto_session.match( value )
                        if search is not None:
                                args.masto_session = unquote( search.group(1) )
                                break
                else:
                        print("ERROR: Could not parse the new masto_session, a fatal error.")
                        exit(2)
                
                # grab the CSRF token next
                # should be gzip compressed, but handle the case it is not
                if incoming.headers['Content-Encoding'] == 'gzip':
                        wrapper = gzip.open( incoming, mode='rt' )
                else:
                        wrapper = incoming

                line = wrapper.readline()
                auth_token = None
                while line != '':
                        search = find_auth_token.search( line )
                        if search is not None:
                                auth_token = search.group(1)
                                break
                        line = wrapper.readline()

                if auth_token is None:
                        print("ERROR: Could not parse the authenticity CSRF token, which we need to submit blocks.")
                        exit(3)

                # POST the details to the appropriate URL
                data = urlencode({        'authenticity_token': auth_token, 
                                        'domain_block[domain]': row['domain'],
                                        'domain_block[severity]': row['severity'],
                                        'domain_block[reject_media]': 0,
                                        'domain_block[reject_reports]': 0,
                                        'domain_block[obfuscate]': 0,
                                        'domain_block[private_comment]': row['source'],
                                        'domain_block[public_comment]': row['comment'],
                                        'button': None})
                outgoing = Request( f'{args.target}/admin/domain_blocks', headers=shared_headers | {
                        'Referer':f'{args.target}/admin/domain_blocks/new',
                        'Cookie':f'_mastodon_session={quote(args.masto_session)}; _session_id={quote(args.session_id)}',
                        }, data=data.encode('utf-8') )

                incoming = urlopen( outgoing )

                # save the returned session token for another round
                for key, value in incoming.headers.items():
                        if key != 'set-cookie':
                                continue
                        search = find_masto_session.match( value )
                        if search is not None:
                                args.masto_session = unquote( search.group(1) )
                                break
                else:
                        print("ERROR: Could not parse the new masto_session, a fatal error.")
                        exit(4)

                # loop back for the next domain        
