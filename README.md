# Retrieve Mastodon Domain Blocks

As of version 4 of Mastodon, instance admins can now publish the domains they've 
 blocked from federating. This is very handy if you're setting up an instance of 
 your own, but unfortunately the results aren't formatted in a way that's easy to
 deal with. This tool is designed to bridge that gap; provide one or more instance
 URIs, and this script will retrieve the domain blocks for you (if they're 
 published, of course), consolidate the duplicates into one, and print them out
 for copy-pasting into your instance setup.

## Dependencies

This script needs `pandas` to function, but otherwise uses standard Python 3 
 libraries.

## Usage

```
retrieve_domain_blocks.py [-h] [-f {expanded,condensed,v4}] [-m] [-s] [-d] [-o [DOMAIN ...]] [--target URI] [--session_id STRING] [--masto_session STRING] URI [URI ...]

```

`--help` or `-h` will get you a summary of all options.

`--format` or `-f` controls the printed format. "expanded" print all available 
 information on their own lines. On most Linux terminal emulators, you can 
 select one of those by triple-clicking, then typing Control-Shift-C to copy. 
 "condensed" prints a subset of the information on a single line, which isn't 
 as convenient for copy-pasting but looks better. "v4" prints something that
 can be imported directly into Mastodon as of version 4 (though the 
 #private_comment column is currently ignored, and as no information on 
 limitations is returned it defaults to placing no limits on media/reports).

`--maximize_severity` or `-m` is a flag which tells the script how to handle
 instances which disagree on how they block a domain. Adding this option will
 go with the harshest block (suspend), while not using it will go with the
 softest (silence).

`-skip_silenced` or `-s` will refuse to print out domains that are "silenced."
 Since the current specifications don't describe the limits placed on a
 silenced instance, it can be hard to figure out what limits you'll place on
 them. It's sensible to say "why bother?" and discover why they were limited
 by yourself.

`--DNS_check` or `-d` will perform a simple DNS look-up for each of the
 domains. This is very handy for detecting censored domain names, but it
 treats domains without any IPv4 address assigned as absent and may thus lead
 to false negatives. It also adds a second or two of runtime per domain
 checked.

`--override` or `-o` lists one or more domains that you would not like to be
 restricted in any way. Only an exact match counts, no wildcards are allowed.
 Separate each domain by a space, and ensure it isn't the last option present
 before the URIs. If using this option, ensure it isn't the last one before 
 the URIs.

`--target`, `--session_id`, and `--masto_session` are an experimental feature
 that will automatically apply the resulting domain block list to the provided
 instance. There are a lot of caveats here: this method has only been tested with
 3.5.5, and has been obsoleted by 4.0; since the API doesn't return what sort of
 limits are placed on a silent domain, this information will be left blank on 
 upload; it requires two cookies pulled from a valid HTTP session of an 
 administrator; because of that last point, it will break (likely without error!)
 if that session is used while domains are being uploaded. In other words,
 don't use this unless you know exactly how it works and are confident it will
 work.

Instance URIs are simply the URL you'd visit to see their front page, ie.
'https://mastodon.social/'. The script will figure out the rest for you.

## Tips

You'll want to pipe the output into `less` or direct it to a file; in most
 cases, most of the output will scroll off screen before you're aware of it.

Domains that aren't running version 4 of Mastodon or don't publish their
 domain blocks will be ignored by the script, so there's no harm in including
 them. What sort of silencing was done (ie. media files or reports rejected)
 isn't included in the output, and currently there's no way to discover that
 unless those details were included in a comment.
 no way to determine that.
